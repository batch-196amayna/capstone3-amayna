import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import AdminNavbar from './components/AdminNavbar';
import ProductView from './components/ProductView';
import AddProduct from './components/AddProduct';
import UpdateProduct from './components/UpdateProduct';
import ArchiveProduct from './components/ArchiveProduct';
import OrderView from './components/OrderView';
import AllOrderView from './components/AllOrderView';
import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminHome from './pages/AdminHome';
import AdminProduct from './pages/AdminProduct';
import Profile from './pages/Profile';

import './App.css';
import {UserProvider} from './UserContext';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  }); 

  const unsetUser = () => {
    localStorage.clear();
  }; 

  useEffect(()=>{
    fetch('https://glacial-temple-19851.herokuapp.com/users/getUserDetails', {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=> res.json())
    .then(data =>{
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data.id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null,
        });
      };
    });
  },[]);

    return(
      
      <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          {(user.isAdmin === true)?
            <>
            <AdminNavbar/>
            <Container>
              <Routes>
                <Route exact path="/" element={<AdminHome/>}/>
                <Route exact path="/products" element={<AdminProduct/>}/>
                <Route exact path="/products/addProduct" element={<AddProduct/>}/>
                <Route exact path="/products/updateProduct/:productId" element={<UpdateProduct/>}/>
                <Route exact path="/products/archiveProduct/:productId" element={<ArchiveProduct/>}/>
                <Route exact path="/profile" element={<Profile/>}/>
                <Route exact path="/getAllOrder" element={<AllOrderView/>}/>
                <Route exact path="/register" element={<Register/>}/>
                <Route exact path="/login" element={<Login/>}/>
                <Route exact path="/logout" element={<Logout/>}/>
                <Route exact path="*" element={<Error/>}/>
              </Routes>
            </Container>
            </>
            :
            <>
            <AppNavbar/>
            <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
              <Route exact path="/products" element={<Products/>}/>
              <Route exact path="/profile" element={<Profile/>}/>
              <Route exact path="/productview/:productId" element={<ProductView/>}/>
              <Route exact path="/register" element={<Register/>}/>
              <Route exact path="/getUserOrder" element={<OrderView/>}/>
              <Route exact path="/login" element={<Login/>}/>
              <Route exact path="/logout" element={<Logout/>}/>
              <Route exact path="*" element={<Error/>}/>
            </Routes>
          </Container>
          </>
        }
        </Router>
      </UserProvider>
      </>
    )
}

export default App;