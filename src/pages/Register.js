import {Navigate, useNavigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import{Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);



	function registerUser(e){
		e.preventDefault();

		fetch('https://glacial-temple-19851.herokuapp.com/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if (data){
				Swal.fire({
					title:"DUPLICATE EMAIL FOUND",
					icon:"info",
					text:"The email that you're trying to registering is already exists!"
				});
				history("/login");
			} else {
				fetch('https://glacial-temple-19851.herokuapp.com/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						mobileNo:mobileNo,
						email:email,
						password:password
					})
				})
				.then(res=>res.json())
				.then(data =>{

					if(data.email){
						Swal.fire({
							title:"REGISTRATION SUCCESSFUL",
							icon:"info",
							text:"Thank you for registering"
						})
					} else {
						Swal.fire({
							title:"REGISTRATION FAILED",
							icon:"error",
							text:"Something went wrong. Please try again later."
						})
					}

				})
			}
		})

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');

	};

	useEffect(()=>{
		if(firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName, lastName, mobileNo, email, password]);

	return(
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		<>
		<h1>Register Here</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label><h4>First Name</h4></Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter your first name here"
				required
				value={firstName}
				onChange={e=> setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label><h4>Last Name</h4></Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter your last name here"
				required
				value={lastName}
				onChange={e=> setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label><h4>Mobile Number</h4></Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter your 11-digit mobile number here"
				required
				value={mobileNo}
				onChange={e=> setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label><h4>Email Address</h4></Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label><h4>Password</h4></Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value={password}
				onChange={e=> setPassword(e.target.value)}
				/>
			</Form.Group>


			{isActive?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Register</Button>
			}
		</Form>
		</>
	)
}
