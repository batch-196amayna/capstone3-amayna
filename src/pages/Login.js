import {useState, useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		fetch('https://glacial-temple-19851.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title:"LOGIN SUCCESSFUL",
					icon:"success",
					text:"Welcome to Book Ordering Application!"
				})
			} else {
				Swal.fire({
					title:"AUTHENTICATION FAILED",
					icon:"error",
					text:"Please check your login credentials"
				})
			}
		});
		
		setEmail('');
		setPassword('');

	};

	const retrieveUserDetails = (token)=>{
		fetch('https://glacial-temple-19851.herokuapp.com/users/getUserDetails',{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			});
		})
	};

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password]);

	return(
		(user.isAdmin === true)?
			<Navigate to="/"/>
		:
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		
		
		<>
		<h1>Login Here</h1>
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label><h3>Email Address</h3></Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label><h3>Password</h3></Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value={password}
				onChange={e=> setPassword(e.target.value)}
				/>
			</Form.Group>

			{isActive?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn" >Login</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Login</Button>
			}
		</Form>
		</>
		
	)
}
