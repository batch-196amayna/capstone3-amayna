import {useEffect, useState, useContext} from 'react'
import {Container, Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Profile(){

	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");

	useEffect(() =>{
		fetch(`https://glacial-temple-19851.herokuapp.com/users/getUserDetails`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setFirstName(data.firstName); 
			setLastName(data.lastName); 
			setMobileNo(data.mobileNo); 
			setEmail(data.email); 
		});
	}, []);


	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>User Profile</Card.Title>
							<Card.Subtitle>First Name</Card.Subtitle>
							<Card.Text>{firstName}</Card.Text>
							<Card.Subtitle>Last Name</Card.Subtitle>
							<Card.Text>{lastName}</Card.Text>
							<Card.Subtitle>Mobile Number</Card.Subtitle>
							<Card.Text>{mobileNo}</Card.Text>
							<Card.Subtitle>Email Address</Card.Subtitle>
							<Card.Text>{email}</Card.Text>

							{(user.isAdmin===true)?
								<Link className="btn btn-primary m-3" to="/getAllOrder">View All Order</Link>
							:						
								<Link className="btn btn-primary m-3" to="/getUserOrder">View Order</Link>
							}

							<Link className="btn btn-primary m-3" to="/">Back to Home</Link>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

