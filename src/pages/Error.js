import {useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';

export default function Error(){
	const navigate = useNavigate();
	const Home = () => {
		navigate('/');
	};
	
	return(

		<Form>
			<h1>404 - Not Found</h1>
			<p>The page you are looking for cannot be found</p>
			<Button className="mt-3 mb-5" onClick={Home} variant="primary">Back to Home</Button>

		</Form>
	
	)
}