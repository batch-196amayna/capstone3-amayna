import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import AdminProductCard from '../components/AdminProductCard';

export default function AdminProduct(){
	const [product, setProduct] = useState([]); 
	 useEffect(()=>{
	 	fetch ('https://glacial-temple-19851.herokuapp.com/products')

	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data);

	 		setProduct(data.map(product => {
	 			return(
	 				<AdminProductCard key={product._id} AdminProductProps={product}/>
	 			)
	 		}))
	 	})
	 }, [])
	return(
			<>
				<Link className="btn btn-primary m-3" to={"/products/addProduct"}>Add Novel</Link>
				<h1>Available Books:</h1>
				
				{product}
			</>
		)
}