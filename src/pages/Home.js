import React from 'react';
import Background from '../components/images/bannerBG.jpg';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
	return(
		<div style = {{backgroundImage:`url(${Background}`}}>
		<>
			<Banner/>
			<Highlights/>
		</>
		</div>
	)
}