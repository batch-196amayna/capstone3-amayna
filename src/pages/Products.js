import {useState, useEffect} from 'react';
import ProductCard from '../components/ProductCard';

export default function Product(){
	const [product, setProduct] = useState([]); 
	 useEffect(()=>{
	 	fetch ('https://glacial-temple-19851.herokuapp.com/products/active')

	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data);

	 		setProduct(data.map(product => {
	 			return(
	 				<ProductCard key={product._id} productProps={product}/>
	 			)
	 		}))
	 	})
	 }, [])
	return(
			<>
				<h1>Available Books:</h1>
				{product}
			</>
		)
}