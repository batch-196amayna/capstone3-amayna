import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import OrderCard from '../components/OrderCard';

export default function OrderView(){

	const [purchased, setPurchased] = useState([]); 

	 useEffect(()=>{
	 	fetch ('https://glacial-temple-19851.herokuapp.com/users/getUserOrder',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data.order);

	 		setPurchased(data.order.map(purchased => {
	 			return(
	 				<OrderCard key={purchased._id} orderProps={purchased}/>
	 			)
	 		}))
	 	})
	 }, [])
	return(
			<>
				<h1>Your Order(s):</h1>
				<Link className="btn btn-primary" to={"/"}>Back to Home</Link>
				{purchased}
			</>
		)
}