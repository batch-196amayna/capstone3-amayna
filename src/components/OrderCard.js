import {Card} from 'react-bootstrap';

export default function OrderCard({orderProps}){

	const {productId, datePurchased} = orderProps

	return(
	<Card className="cardProduct p-2 m-3">
		<Card.Body>
			<Card.Subtitle>Order ID</Card.Subtitle>
			<Card.Text>
				{productId}
			</Card.Text>
			<Card.Subtitle>Date Purchased</Card.Subtitle>
			<Card.Text>{datePurchased}</Card.Text>
		</Card.Body>
	</Card>
	)
};