import{Link, useNavigate} from 'react-router-dom';
import{useEffect, useState} from 'react';
import{Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddProduct() {

	const history = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);

	function addProduct(e){
		e.preventDefault();

		fetch('https://glacial-temple-19851.herokuapp.com/products/checkProductNameExists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name:name
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if (data){
				Swal.fire({
					title: "DUPLICATE FOUND",
					icon: "info",
					text: "The novel that you're trying to add is already exists!"
				});
				history("/products");
			} else {
				fetch('https://glacial-temple-19851.herokuapp.com/products/addProduct', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						name:name,
						description:description,
						price:price
					})
				})
				.then(res=>res.json())
				.then(data=>{
					if(data.name){
						Swal.fire({
							title:"SUCCESS",
							icon:"success",
							text:"Novel has been added"
						})
					} else {
						Swal.fire({
							title:"FAILED",
							icon:"error",
							text:"Something went wrong. Please try again later."
						})
					}
				})
			}
		})

		setName('');
		setDescription('');
		setPrice('');
	};

	useEffect(()=>{
		if (name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[name, description, price]);

	return(
		<>
		<h1>Add New Novel</h1>

		<Form onSubmit={e => addProduct(e)}>
			<Form.Group controlId="name">
				<Form.Label>Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter the novel title here"
						required
						value={name}
						onChange={e=> setName(e.target.value)}
						/>
			</Form.Group>
			<Form.Group controlId="description">
				<Form.Label>Description</Form.Label>
					<Form.Control
						as="textarea"
						rows={3}
						placeholder="Enter the novel description here"
						required
						value={description}
						onChange={e=> setDescription(e.target.value)}
						/>
			</Form.Group>
			<Form.Group controlId="price">
				<Form.Label>Price</Form.Label>
					<Form.Control
						type="number"
						placeholder="Enter the novel price here"
						required
						value={price}
						onChange={e=> setPrice(e.target.value)}
						/>
			</Form.Group>

			{isActive?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Add Novel</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Add Novel</Button>
			}<br/>
			<Link className="btn btn-primary mt-3" to={`/products`}>View Novel</Link>
		</Form>
		</>
	)
}