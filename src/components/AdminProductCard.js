import {Card, Button} from 'react-bootstrap';
import {useNavigate, Link} from 'react-router-dom';


export default function ProductCard({AdminProductProps}){

	const {name, description, price, _id, isActive} = AdminProductProps


	const navigate = useNavigate();
	const Archive = () => {
		navigate(`/products/archiveProduct/${_id}`);
	};
	const Retrive = () => {
		navigate(`/products/updateProduct/${_id}`);
	};

	return(
	<Card className="cardProduct p-2 m-3">
		<Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Subtitle>Description</Card.Subtitle>
			<Card.Text>
				{description}
			</Card.Text>
			<Card.Subtitle>Price</Card.Subtitle>
			<Card.Text>{price}</Card.Text>
			<Link className="btn btn-primary m-3" to={`/products/updateProduct/${_id}`}>Update</Link>
			{isActive?
			<Button className="m-3" variant="primary" type="submit" id="archiveBtn" onClick={Archive}>Archive</Button>
			:
			<Button className="m-3" variant="danger" type="submit" id="retriveBtn" onClick={Retrive}>Retrive</Button>
			}
		</Card.Body>
	</Card>
	
	)
};