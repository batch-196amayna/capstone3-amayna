import {Card} from 'react-bootstrap';

export default function AllOrderCard({allOrderProps}){

	const {firstName, lastName, productId, datePurchased} = allOrderProps

	return(
	<Card className="cardProduct p-2 m-3">
		<Card.Body>
			<Card.Subtitle>User Name</Card.Subtitle>
			<Card.Text>{`${firstName} ${lastName}`}</Card.Text>
			<Card.Subtitle>Order ID</Card.Subtitle>
			<Card.Text>{productId}</Card.Text>
			<Card.Subtitle>Date Purchased</Card.Subtitle>
			<Card.Text>{datePurchased}</Card.Text>
		</Card.Body>
	</Card>
	)
};