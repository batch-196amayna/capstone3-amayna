import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProps}){

	const {name, description, price, _id} = productProps

	return(
	<Card className="cardProduct p-2 m-3">
		<Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Subtitle>Description</Card.Subtitle>
			<Card.Text>
				{description}
			</Card.Text>
			<Card.Subtitle>Price</Card.Subtitle>
			<Card.Text>{price}</Card.Text>
			<Link className="btn btn-primary" to={`/productview/${_id}`}>View Novel</Link>
		</Card.Body>
	</Card>
	
	)
};