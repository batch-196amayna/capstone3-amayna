import {useEffect, useState} from 'react'
import {Form, Button, Col, Card} from 'react-bootstrap';
import {Link, useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ArchiveProduct(){


	const history = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

		const{productId} = useParams();

	useEffect(() =>{
		fetch(`https://glacial-temple-19851.herokuapp.com/products/getSingleProduct/${productId}`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setName(data.name); 
			setDescription(data.description); 
			setPrice(data.price); 
		});
	}, []);

	function archiveProduct(e){
		e.preventDefault();

		fetch(`https://glacial-temple-19851.herokuapp.com/products/archiveProduct/${productId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data) {
				Swal.fire({
					title:'SUCCESSFULLY DELETED',
					icon: 'success',
					text: 'Novel is no longer available'
				});
				history("/products");
			} else {
				Swal.fire({
					title:'SOMETHING WENT WRONG',
					icon: 'error',
					text: 'Please try again later'
				});
			};
		});
		setName('');
		setDescription('');
		setPrice('');
	};

	return(
		<>
		<Form onSubmit={e => archiveProduct(e)}>
			<Col lg={{span:6, offset:3}}>
				<Card>
					<Card.Body>
						<Card.Subtitle>Name</Card.Subtitle>
						<Card.Text>{name}</Card.Text>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn">Delete</Button><br/>
						<Link className="btn btn-primary mt-3" to={`/products`}>View Novel</Link>
					</Card.Body>
				</Card>
			</Col>
		</Form>
		</>
	)
}

