import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext'; 

export default function ProductView(){

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const{productId} = useParams();

	const order = (productId) =>{
		fetch('https://glacial-temple-19851.herokuapp.com/users/order', {
			method: 'POST',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data) {
				Swal.fire({
					title:'SUCCESSFULLY ORDERED',
					icon: 'success',
					text: 'Thank you for ordering'
				});
				history("/products");
			} else {
				Swal.fire({
					title:'SOMETHING WENT WRONG',
					icon: 'error',
					text: 'Please try again later'
				});
			};
		});
	};

	useEffect(() =>{
		fetch(`https://glacial-temple-19851.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setName(data.name); 
			setDescription(data.description); 
			setPrice(data.price); 
		})
	}, [productId]);
	
	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							{user.id !== null?
								<Button variant="primary" onClick={()=> order(productId)}>Order</Button>
								:
								<Link className="btn btn-danger" to="/login">Log In</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}