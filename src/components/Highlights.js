import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3 bg-info">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 bg-dark text-light">
					<Card.Body>
						<Card.Title>
							<h2>Who Made Me A Princess</h2>
						</Card.Title>
						<Card.Text>
							The beautiful Athanasia was killed at the hands of her own biological father, Claude de Alger Obelia, the cold-blooded emperor! It’s just a silly bedtime story… until one woman wakes up to suddenly find she’s become that unfortunate princess! She needs a plan to survive her doomed fate, and time is running out. Will she go with Plan A, live as quietly as possible without being noticed by the infamous emperor? Plan B, collect enough money to escape the palace? Or will she be stuck with Plan C, sweet-talking her way into her father’s good graces?!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 bg-dark text-light">
					<Card.Body>
						<Card.Title>
							<h2>Actually I Am The Real One</h2>
						</Card.Title>
						<Card.Text>
							A prophecy foretold that the Grand Duke would only have one water elementalist born from him. Though she lacks her father’s affection, Keira knows she’s destined for those powers. But after years of doing what was right and proper of a noble, she’s executed after the conniving Cosette appears, claiming to be the Grand Duke’s real daughter. But then—Keira wakes up with newfound purpose: make changes, allies, and even friends before it’s too late. Will it be enough before Cosette hatches her plot?
						</Card.Text>
					</Card.Body>
				</Card>
				
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 bg-dark text-light">
					<Card.Body>
						<Card.Title>
							<h2>Death Is The Only Way For Villainess</h2>
						</Card.Title>
						<Card.Text>
							This game’s got me obsessed! It’s like it’s taken over my life… wait—HARD MODE ACTIVATED. No-no-no-no-no! It has literally taken me and thrown me into the part of Penelope Eckhart. Love is easy as the heroine, but as the hated villainess, I’m trying very hard not to die… at the hands of my brothers… the prince… a fork… every possible ending is death! This world is stacked against me, but can my wits and insider game knowledge score the affection of these male characters? Or the reset button???
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
};