import React from 'react';
import {useNavigate} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';
import Background from './images/bannerBG.jpg';

export default function Banner() {

	const navigate = useNavigate();
	const Product = () => {
		navigate('/products');
	};

	return(
		<div style = {{backgroundImage:`url(${Background}`}}>
		<Row>
			<Col className = "p-5">
				<h1>Book Ordering App</h1>
				<h3>Readings for everyone, everywhere!</h3>
				<Button variant="warning" onClick={Product}>Order Now</Button>
			</Col>
		</Row>
		</div>
	)
}