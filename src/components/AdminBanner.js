import React from 'react';
import {useNavigate} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';

export default function AdminBanner() {

	const navigate = useNavigate();
	const Product = () => {
		navigate('/products');
	};

	return(
		<div>
		<Row>
			<Col className = "p-5">
				<h1>Hello Admin</h1>
				<h3>Book Ordering App</h3>
				<p>Readings for everyone, everywhere!</p>
				<Button variant="warning" onClick={Product}>View Products</Button>
			</Col>
		</Row>
		</div>
	)
}