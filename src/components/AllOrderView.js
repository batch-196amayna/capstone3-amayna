import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import AllOrderCard from '../components/AllOrderCard';

export default function AllOrderView(){

	const [userPurchased, setUserPurchased] = useState([]); 
	const [orderPurchased, setOrderPurchased] = useState([]); 

	 useEffect(()=>{
	 	fetch ('https://glacial-temple-19851.herokuapp.com/users/getAllOrder',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data.order)

	 		setUserPurchased(data.map(userPurchased => {
	 			return(
	 				<AllOrderCard key={userPurchased._id} allOrderProps={userPurchased}/>
	 			)

	 		}))

	 		setOrderPurchased(data.order.map(orderPurchased => {
			 	return(
			 		<AllOrderCard key={orderPurchased._id} allOrderProps={orderPurchased}/>
			 	)
	 		}))

	 	})
	 }, [])
	return(
			<>
				<h1>All Order(s):</h1>
				<Link className="btn btn-primary" to={"/"}>Back to Home</Link>
				{userPurchased}
				{orderPurchased}
			</>
		)
}