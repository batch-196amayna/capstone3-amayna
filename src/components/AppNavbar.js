import {useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	return (
	<Navbar bg="info" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Erica</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto"> 
	          <Nav.Link as={Link} to="/">Home</Nav.Link>
	          <Nav.Link as={Link} to="/products">Novels</Nav.Link>

            {
              (user.id !== null) ?
                <>
                  <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                </>
              :
                <> 
                  <Nav.Link as={Link} to="/login">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register">Register</Nav.Link>
                </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	)
}
